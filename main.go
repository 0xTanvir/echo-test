package main

import (
	"net/http"
	"sort"

	"github.com/labstack/echo"
	"gopkg.in/go-playground/validator.v9"
)

type Emails struct {
	Emails     []string `json:"emails" validate:"required"`
	ValidEmail string   `json:"valid_email" validate:"required,email"`
}

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

func main() {
	e := echo.New()
	e.Validator = &CustomValidator{validator: validator.New()}

	e.POST("/sort", sortEmail)

	e.Logger.Fatal(e.Start(":1323"))
}

func sortEmail(c echo.Context) error {

	emails := new(Emails)
	if err := c.Bind(emails); err != nil {
		return err
	}
	if err := c.Validate(emails); err != nil {
		return err
	}

	sort.Strings(emails.Emails)

	return c.JSON(http.StatusOK, emails.Emails)
}
